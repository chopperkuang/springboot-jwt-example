package io.kuang.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

/**
 * Created by chopperkuang on 2017/6/21.
 *
 * @author 闷骚乔巴
 * @since 2017/6/21
 */
@SpringBootApplication
@ServletComponentScan(value = "io.kuang.example.filter")
public class SimpleJwtApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimpleJwtApplication.class, args);
    }
}
