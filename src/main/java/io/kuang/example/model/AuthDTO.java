package io.kuang.example.model;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by chopperkuang on 2017/6/21.
 *
 * @author 闷骚乔巴
 * @since 2017/6/21
 */
@Data
@AllArgsConstructor
public class AuthDTO {
    private String userName;
    private String password;
}
