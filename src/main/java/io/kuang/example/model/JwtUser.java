package io.kuang.example.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by chopperkuang on 2017/6/21.
 *
 * @author 闷骚乔巴
 * @since 2017/6/21
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class JwtUser {
    private String userName;
}