package io.kuang.example.model;

import lombok.Builder;
import lombok.Data;

/**
 * Created by chopperkuang on 2017/6/21.
 *
 * @author 闷骚乔巴
 * @since 2017/6/21
 */
@Data
@Builder
public class User {

    private String userName;
    private String passWord;
    private String email;
    private String description;
    private Boolean isActivated;
}