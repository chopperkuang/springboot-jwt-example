package io.kuang.example.controller;

import io.kuang.example.model.AuthDTO;
import io.kuang.example.model.JwtUser;
import io.kuang.example.service.JwtService;
import io.kuang.example.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by chopperkuang on 2017/6/21.
 *
 * @author 闷骚乔巴
 * @since 2017/6/21
 */
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private JwtService jwtService;

    @GetMapping(value = "/api/secure/hello/{name}")
    public ResponseEntity<?> helloSecure(@PathVariable String name, HttpServletRequest request) {
        System.out.println(request.getAttribute("jwtUser"));
        String result = String.format("Hello JWT, %s! (Secure)", name);
        return ResponseEntity.ok(result);
    }

    @GetMapping(value = "/v1/api/current")
    public ResponseEntity<?> current(HttpServletRequest request) {
        return ResponseEntity.ok(request.getAttribute("jwtUser"));
    }

    @GetMapping(value = "/api/public/hello/{name}")
    public ResponseEntity<?> helloPublic(@PathVariable String name) {
        String result = String.format("Hello JWT, %s! (Public)", name);
        return ResponseEntity.ok(result);
    }

    @PostMapping(value = "/api/public/auth")
    public ResponseEntity<?> auth(@RequestBody AuthDTO auth) {
        String userName = auth.getUserName();
        String password = auth.getPassword();
        Boolean correctCredentials = userService.authenticate(userName, password);
        if (correctCredentials) {
            JwtUser jwtUser = new JwtUser(userName);
            return ResponseEntity.ok(jwtService.getToken(jwtUser));
        }
        return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
    }
}
